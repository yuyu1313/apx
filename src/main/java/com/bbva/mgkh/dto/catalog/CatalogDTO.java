package com.bbva.mgkh.dto.catalog;

import java.sql.Timestamp;

import com.bbva.apx.dto.AbstractDTO;

public class CatalogDTO extends AbstractDTO{
	  private static final long serialVersionUID =1L;
	  private String type;
	  private String code;
	  private String value;
	  private String description;
	  private String status;
	  private Timestamp currentDate;
	  private Timestamp modifiedDate;
	  private String currentUser;
	  private String modifiedUser;
	  
	  public CatalogDTO(){}
	  
	  public CatalogDTO(String type,String code, String value, String description, String status,String currentUser, Timestamp currentDate, String modifiedUser,Timestamp modifiedDate){
		  super();
		  this.type = type;
		  this.code = code;
		  this.value = value;
		  this.description = description;
		  this.status = status;
		  this.currentUser = currentUser;
		  this.currentDate = currentDate;
		  this.modifiedUser = modifiedUser;
		  this.modifiedDate = modifiedDate;		   
		  
	  }
	  
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(Timestamp currentDate) {
		this.currentDate = currentDate;
	}

	public Timestamp getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(String currentUser) {
		this.currentUser = currentUser;
	}

	public String getModifiedUser() {
		return modifiedUser;
	}

	public void setModifiedUser(String modifiedUser) {
		this.modifiedUser = modifiedUser;
	}

	
	  
	  

}
